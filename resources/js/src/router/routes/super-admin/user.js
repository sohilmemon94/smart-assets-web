export default [
  // *===============================================---*
  // *--------- USER ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/admin/my-profile',
    name: 'super-admin-myProfile',
    component: () => import('@/views/super-admin/my-profile/MyProfile.vue'),
    meta: {
      pageTitle: 'My Profile',
      breadcrumb: [
        {
          text: 'My Profile',
          active: true,
        },
      ],
    },
  },
  {
    path: '/admin/user/list',
    name: 'admin-user-list',
    component: () => import('@/views/super-admin/user/user-list/UserList.vue'),
    meta: {
      pageTitle: 'Super Admin User List',
      breadcrumb: [
        {
          text: 'Super Admin User',
          to : { name: 'admin-user-list'}
        },
        {
          text: 'Super Admin User List',
          active: true,
        },
      ],
    },
  },
  {
    path: '/admin/user/add',
    name: 'admin-user-add',
    component: () => import('@/views/super-admin/user/user-add/Create.vue'),
    meta: {
      navActiveLink: 'admin-user-list',
      pageTitle: 'Admin User Add',
      breadcrumb: [
        {
          text: 'Admin User',
          to : { name: 'admin-user-list'}
        },
        {
          text: 'Admin User Add',
          active: true,
        },
      ],
    },
  },
  {
    path: '/admin/user/view/:id',
    name: 'admin-user-view',
    component: () => import('@/views/super-admin/user/user-view/UserView.vue'),
    meta: {
      navActiveLink: 'admin-user-list',
      pageTitle: 'Admin User View',
      breadcrumb: [
        {
          text: 'Admin User',
          to : { name: 'admin-user-list'}
        },
        {
          text: 'Admin User View',
          active: true,
        },
      ],
    },
  },
  {
    path: '/admin/user/edit/:id',
    name: 'admin-user-edit',
    component: () => import('@/views/super-admin/user/user-edit/UserEdit.vue'),
    meta: {
      navActiveLink: 'admin-user-list',
      pageTitle: 'Admin User Edit',
      breadcrumb: [
        {
          text: 'Admin User',
          to : { name: 'admin-user-list'}
        },
        {
          text: 'Admin User Edit',
          active: true,
        },
      ],
    },
  },
]
