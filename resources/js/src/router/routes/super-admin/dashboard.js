export default [
    {
        path: '/admin/dashboard',
        name: 'admin-dashboard',
        component: () => import('@/views/common/dashboard/home/Dashboard.vue'),
    },
    {
        path: '/admin/dashboard/details/:apiKey',
        props: true,
        name: 'admin-dashboard-details',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'admin-dashboard',
            // pageTitle: 'Detail Listing',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'admin-dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/admin/dashboard/details/:apiKey/:time',
        props: true,
        name: 'admin-dashboard-details-time',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'admin-dashboard',
            // pageTitle: 'Detail Listing',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'admin-dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/admin/dashboard/asset-info/:id',
        props: true,
        name: 'admin-dashboard-asset-details',
        component: () => import('@/views/common/dashboard/detail-info/Index.vue'),
        meta: {
            navActiveLink: 'admin-dashboard',
            pageTitle: 'Asset Info',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'admin-dashboard' }
                },
                {
                    text: 'Details'
                },
                {
                    text: 'Asset Info',
                    active: true,
                },
            ],
        },
    }
]
