export default [
    // *===============================================---*
    // *--------- CLIENT ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/client/list',
      name: 'admin-client-list',
      component: () => import('@/views/super-admin/client/client-list/ClientList.vue'),
      meta: {
        pageTitle: 'Client Admin User List ',
        breadcrumb: [
          {
            text: 'Client Admin',
            to : { name: 'admin-client-list'}
          },
          {
            text: 'Client Admin User List',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/client/add',
      name: 'admin-client-add',
      component: () => import('@/views/super-admin/client/client-add/Create.vue'),
      meta: {
        navActiveLink: 'admin-client-list',
        pageTitle: 'Add Client',
        breadcrumb: [
          {
            text: 'client',
            to : { name: 'admin-client-list'}
          },
          {
            text: 'Add Client',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/client/view/:id',
      name: 'admin-client-view',
      component: () => import('@/views/super-admin/client/client-view/ClientView.vue'),
      meta: {
        navActiveLink: 'admin-client-list',
        pageTitle: 'Client View',
        breadcrumb: [
          {
            text: 'client',
            to : { name: 'admin-client-list'}
          },
          {
            text: 'Client View',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/client/edit/:id',
      name: 'admin-client-edit',
      component: () => import('@/views/super-admin/client/client-edit/ClientEdit.vue'),
      meta: {
        navActiveLink: 'admin-client-list',
        pageTitle: 'Client Edit',
        breadcrumb: [
          {
            text: 'client',
            to : { name: 'admin-client-list'}
          },
          {
            text: 'Client Edit',
            active: true,
          },
        ],
      },
    },
  ]
