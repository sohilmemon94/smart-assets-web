import dashboard from './dashboard'
// import report from './report'
import user from './user'
import category from './category'
import client from './client'
import module from './module'
import template from './template'
import company from './company'
import globalConfiguration from './global-configuration'

const data = [...dashboard, ...user, ...category, ...client, ...module, ...template, ...company, ...globalConfiguration]
// const data = [...dashboard, ...report, ...user, ...category, ...client, ...module, ...template]

export default data
