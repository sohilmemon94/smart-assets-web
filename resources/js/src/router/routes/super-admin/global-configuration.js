export default [
  // *===============================================---*
  // *--------- USER ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/admin/global-configuration',
    name: 'super-admin-global-configuration',
    component: () => import('@/views/super-admin/global-configuration/index.vue'),
    meta: {
      pageTitle: 'Global Configuration',
      breadcrumb: [
        {
          text: 'Global Configuration',
          active: true,
        },
      ],
    },
  },
]
