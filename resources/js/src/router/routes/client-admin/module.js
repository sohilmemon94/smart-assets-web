export default [
    // *===============================================---*
    // *--------- MODULE ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/module/list',
        name: 'client-module-list',
        component: () =>
            import ('@/views/client-admin/module/module-list/ModuleList.vue'),
        meta: {
            pageTitle: 'Module List',
            breadcrumb: [{
                    text: 'Module',
                },
                {
                    text: 'Module List',
                    active: true,
                },
            ],
        },
    },
     // *===============================================---*
    // *--------- MODULE FIELD LIST ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/module/list/:id?',
        name: 'client-module-field',
        component: () => import('@/views/client-admin/module/module-field-list/ModuleFieldList.vue'),
        meta: {
          navActiveLink: 'client-module-list',
          pageTitle: 'Module Field List',
          breadcrumb: [
            {
              text: 'Module Field',
            },
            {
              text: 'Module Field List',
              active: true,
            },
          ],
        },
      },
    {
        path: '/client/category/:category_id/module/list/',
        name: 'client-category-module-list',
        component: () => import('@/views/client-admin/module/module-list/ModuleList.vue'),
        meta: {
          navActiveLink: 'client-module-list',
          pageTitle: 'Module List',
          breadcrumb: [
            {
              text: 'Category',
            },
            {
              text: 'Module',
            },
            {
              text: 'Module List',
              active: true,
            },
          ],
        },
      },
      // *===============================================---*
    // *--------- MODULE QUESTION LIST ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/client/module/:id/question',
      name: 'client-module-question',
      component: () => import('@/views/client-admin/module/module-question-list/ModuleQuestionList.vue'),
      meta: {
        navActiveLink: 'client-module-list',
        pageTitle: 'Module Question List',
        breadcrumb: [
          {
            text: 'Module Question'
          },
          {
            text: 'Module Question List',
            active: true,
          },
        ],
      },
    },
    {
      path: '/client/module/:id/question/add',
      name: 'client-module-question-add',
      component: () => import('@/views/client-admin/module/module-question-add/ModuleQuestionAdd.vue'),
      meta: {
        navActiveLink: 'client-module-list',
        pageTitle: 'Add Module Question',
        breadcrumb: [
          {
            text: 'Module Question',
          },
          {
            text: 'Add Module Question',
            active: true,
          },
        ],
      },
    },
    {
      path: '/client/module/:id/question/edit/:msq_id',
      name: 'client-module-question-edit',
      component: () => import('@/views/client-admin/module/module-question-edit/ModuleQuestionEdit.vue'),
      meta: {
        navActiveLink: 'client-module-list',
        pageTitle: 'Module Question Edit',
        breadcrumb: [
          {
            text: 'Module Question',
          },
          {
            text: 'Module Question Edit',
            active: true,
          },
        ],
      },
    },
    // *===============================================---*
    // *--------- MODULE QUESTION ANSWER ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/client/module/:id/question-answer',
      name: 'client-module-question-answer',
      component: () => import('@/views/client-admin/module/module-question-answer-list/ModuleQuestionAnswerList.vue'),
      meta: {
        navActiveLink: 'client-module-list',
        pageTitle: 'Module Question Answer List',
        breadcrumb: [
          {
            text: 'Module Question Answer',
          },
          {
            text: 'Module Question Answer List',
            active: true,
          },
        ],
      },
    },
]
