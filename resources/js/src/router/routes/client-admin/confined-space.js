export default [
    // *===============================================---*
    // *--------- Confined Space ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/client/confined-space/list',
      name: 'client-confined-space-list',
      component: () => import('@/views/client-admin/confined-space/confined-space-list/ConfinedSpaceList.vue'),
      meta: {
        pageTitle: 'Confined Space List',
        breadcrumb: [
          {
            text: 'Confined Space',
          },
          {
            text: 'Confined Space List',
            active: true,
          },
        ],
      },
    },
    {
        path: '/client/confined-space/detail/:id',
        name: 'client-confined-space-detail',
        component: () => import('@/views/client-admin/confined-space/confined-space-detail/ConfinedSpaceDetail.vue'),
        meta: {
          navActiveLink: 'client-confined-space-list',
          pageTitle: 'Confined Space Details',
          breadcrumb: [
            {
              text: 'Confined Space',
            },
            {
              text: 'Confined Space Details',
              active: true,
            },
          ],
        },
      },

  ]
