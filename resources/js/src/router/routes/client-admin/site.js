export default [
    // *===============================================---*
    // *--------- SITE ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/location/list',
        name: 'client-site-list',
        component: () =>
            import ('@/views/client-admin/location/location-list/LocationList.vue'),
        meta: {
            pageTitle: 'Location List',
            breadcrumb: [{
                    text: 'Location',
                },
                {
                    text: 'Location List',
                    active: true,
                },
            ],
        },
    }

]
