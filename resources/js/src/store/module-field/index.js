import ModuleFieldServices from '@/services/moduleFieldServices';

export default {
  namespaced: true,
  state: {
    inputFieldType: [],
    moduleFieldList: {
      moduleField: {
        data: [],
        to: 0,
        from: 0
      }
    },
    moduleName: '',
    moduleFieldData: {
      mf_id: '',
      mf_module_id: '',
      mf_name: '',
      mf_input_field_type_id: '',
      mf_is_show_on_grid: '',
      mf_is_show_on_details: '',
      mf_has_expiry_date: '',
      mf_is_required: '',
      mf_status: true,
      created_by: '',
    },
    modalButtonName: 'Add',
    editMode: false,
    viewMode: false,
    modalButtonDisable: false,
  },
  mutations: {
    UPDATE_INPUT_FIELD_TYPE(state, payload) {
      state.inputFieldType = payload;
    },
    UPDATE_MODULE_NAME(state, payload) {
      state.moduleName = payload;
    },
    UPDATE_MODULE_FIELD_LIST(state, payload) {
      state.moduleFieldList = payload;
    },
    UPDATE_MODULE_FIELD_DATA(state, payload) {
      state.moduleFieldData = payload;
    },
    UPDATE_MODAL_BUTTON_NAME(state, val) {
      state.modalButtonName = val
    },
    UPDATE_EDIT_MODE(state, val) {
      state.editMode = val
    },
    UPDATE_VIEW_MODE(state, val) {
      state.viewMode = val
    },
    UPDATE_MODAL_BUTTON_DISABLE(state, val) {
      state.modalButtonDisable = val
    },
  },
  getters: {
    inputFieldType(state) {
      return state.inputFieldType;
    },
    moduleName(state) {
      return state.moduleName;
    },
    moduleFieldList: state => {
      const { moduleFieldList } = state
      return moduleFieldList;
    },
    moduleFieldData: state => {
      const { moduleFieldData } = state
      return moduleFieldData;
    },
    modalButtonName: state => {
      const { modalButtonName } = state
      return modalButtonName;
    },
    editMode: state => {
      const { editMode } = state
      return editMode;
    },
    viewMode: state => {
      const { viewMode } = state
      return viewMode;
    },
    modalButtonDisable: state => {
      const { modalButtonDisable } = state
      return modalButtonDisable;
    },
  },
  actions: {
    // Get all input field type in database
    getAllInputField({ commit }, payload) {
      return ModuleFieldServices.getAllInputField(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_INPUT_FIELD_TYPE', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_INPUT_FIELD_TYPE', []);
          return Promise.reject(error);
        }
      );
    },
    // Get all field based on module id
    getModuleFieldList({ commit }, payload) {
      return ModuleFieldServices.getModuleFieldList(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            // Set module name
            commit('UPDATE_MODULE_NAME', response.data.moduleName);

            commit('UPDATE_MODULE_FIELD_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Save module field data in database
    saveModuleField({ commit, dispatch }, payload) {
      return ModuleFieldServices.saveModuleField(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    // Get specific module field data
    getModuleFieldData({ commit }, payload) {
      return ModuleFieldServices.getModuleFieldData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            response.data.mf_is_show_on_grid = (response.data.mf_is_show_on_grid == 'Y') ? true : false;
            response.data.mf_is_show_on_details = (response.data.mf_is_show_on_details == 'Y') ? true : false;
            response.data.mf_is_required = (response.data.mf_is_required == 'Y') ? true : false;
            response.data.mf_status = (response.data.mf_status == 'Y') ? true : false;
            response.data.mf_has_expiry_date = (response.data.mf_has_expiry_date == 'Y') ? true : false;

            commit('UPDATE_MODULE_FIELD_DATA', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // update module field data in database
    updateModuleField({ commit, dispatch }, payload) {
      return ModuleFieldServices.updateModuleField(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Change module Field Status in database
    changeModuleFieldStatus({ commit, dispatch }, payload) {
      return ModuleFieldServices.changeModuleFieldStatus(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Delete module Field Status in database
    deleteModuleField({ commit, dispatch }, payload) {
      return ModuleFieldServices.deleteModuleField(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Save template field for module in database
    saveTemplateFieldForModule({ commit, dispatch }, payload) {
      return ModuleFieldServices.saveTemplateFieldForModule(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },

  },
}
