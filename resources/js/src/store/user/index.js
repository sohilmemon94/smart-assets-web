import { $themeBreakpoints } from '@themeConfig'
import UserServices from '../../services/userServices';

export default {
  namespaced: true,
  state: {
    roleOptions: [],
    adminUserList: {
      users: {
        data: [],
        to: 0,
        from: 0
      }
    },
    clientUserList: {
      users: {
        data: [],
        to: 0,
        from: 0
      }
    },
    clientMakeAdminList: {
        userList: {
          data: [],
          to: 0,
          from: 0
        }
    },
    siteUserList: {
      users: {
        data: [],
        to: 0,
        from: 0
      }
    },
    userData: {},
    userWithCompanyData: {},
  },
  getters: {
    roleOptions: state => {
      const { roleOptions } = state
      return roleOptions;
    },
    adminUserList: state => {
      const { adminUserList } = state
      return adminUserList;
    },
    clientUserList: state => {
      const { clientUserList } = state
      return clientUserList;
    },
    clientMakeAdminList: state => {
      const { clientMakeAdminList } = state
      return clientMakeAdminList;
    },
    siteUserList: state => {
        const { siteUserList } = state
        return siteUserList;
      },
    userData: state => {
      const { userData } = state
      return userData;
    },
    userWithCompanyData: state => {
        const { userWithCompanyData } = state
        return userWithCompanyData;
      },
  },
  mutations: {
    UPDATE_ROLE_OPTIONS(state, val) {
      state.roleOptions = val
    },
    UPDATE_ADMIN_USER_LIST(state, val) {
      state.adminUserList = val
    },
    UPDATE_CLIENT_USER_LIST(state, val) {
      state.clientUserList = val
    },
    UPDATE_CLIENT_MAKE_ADMIN_LIST(state, val) {
        state.clientMakeAdminList = val
    },
    UPDATE_SITE_USER_LIST(state, val) {
      state.siteUserList = val
    },
    UPDATE_USER_DATA(state, val) {
      state.userData = val
    },
    UPDATE_WITH_COMPANY_DATA(state, val) {
        state.userWithCompanyData = val
      },
  },
  actions: {
    // Get all admin user data
    getAllAdminUser({ commit }, payload) {
      return UserServices.getAllAdminUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_ADMIN_USER_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_ADMIN_USER_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get specific user data
    getUserData({ commit }, payload) {
      return UserServices.getUserData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_USER_DATA', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_USER_DATA', {});
          return Promise.reject(error);
        }
      );
    },

    // Get specific user data
    getUserWithCompanyData({ commit }, payload) {
        return UserServices.getUserWithCompanyData(payload).then(
          response => {
            if (response.status == true || response.status == 200) {
                //console.log(response.data.users);
              commit('UPDATE_WITH_COMPANY_DATA', response.data.users);
              //commit('UPDATE_USER_DATA', response.data.users);
              commit('UPDATE_CLIENT_MAKE_ADMIN_LIST', response.data.userList);
            }
            return Promise.resolve(response);
          },
          error => {
            commit('UPDATE_WITH_COMPANY_DATA', {});
            //commit('UPDATE_USER_DATA', {});
            commit('UPDATE_CLIENT_MAKE_ADMIN_LIST', {});
            return Promise.reject(error);
          }
        );
      },
    // Get new user data in database
    getAllAdminUser({ commit }, payload) {
      return UserServices.getAllAdminUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_ADMIN_USER_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_ADMIN_USER_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get all client user data in database
    getAllClientUser({ commit }, payload) {
      return UserServices.getAllClientUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CLIENT_USER_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_CLIENT_USER_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get all site user data in database
    getAllSiteUser({ commit }, payload) {
      return UserServices.getAllSiteUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_SITE_USER_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_SITE_USER_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Store new user data in database
    storeNewUser({ commit, dispatch }, payload) {
      return UserServices.storeNewUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            //dispatch('GET_CLIENT_ACTIONS');
            //dispatch("GET_CLIENT_OPTION_ACTION");
            //dispatch("CLIENT_DASHBOARD_ACTION");
          }
          return Promise.resolve(response);
        },
        error => {
          // commit('UPDATE_CLIENT_MUTATION', {});
          return Promise.reject(error);
        }
      );
    },
    // update user data in database
    updateUser({ commit, dispatch }, payload) {
      return UserServices.updateUser(payload).then(
        response => {
          if (response.status == true || response.status == 200) {

          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },

    // update user data in database
    updateUserToAdmin({ commit, dispatch }, payload) {
        return UserServices.updateUserToAdmin(payload).then(
          response => {
            if (response.status == true || response.status == 200) {

            }
            return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
        );
    },

    // Change Site Status in database
    changeUserStatus({ commit, dispatch }, payload) {
        return UserServices.changeStatus(payload).then(
        response => {
            return Promise.resolve(response);
        },
        error => {
            return Promise.reject(error);
        }
        );
    },
    // Get all roles
    getAllRole({ commit, dispatch }, payload) {
      return UserServices.getAllRole(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_ROLE_OPTIONS', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_ROLE_OPTIONS', []);

          return Promise.reject(error);
        }
      );
    },
  },
}
