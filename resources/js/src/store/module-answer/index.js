import ModuleAnswerServices from '@/services/moduleAnswerServices.js';


export default {
    namespaced: true,
    state: {
        moduleAnswerList: {
            moduleAnswer: {
                data: [],
                to: 0,
                from: 0
            }
        },
        moduleName: '',
    },
    mutations: {
        UPDATE_MODULE_ANSWER_LIST(state, payload) {
            state.moduleAnswerList = payload;
        },
        UPDATE_MODULE_NAME(state, payload) {
            state.moduleName = payload;
        },
    },
    getters: {
        moduleName(state) {
            return state.moduleName;
          },
        moduleAnswerList: state => {
            const { moduleAnswerList } = state
            return moduleAnswerList;
        },
    },
    actions: {
        // Get all question based on module id
        getModuleAnswerList({ commit }, payload) {
            return ModuleAnswerServices.getModuleAnswerList(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        // Set module name
                        commit('UPDATE_MODULE_NAME', response.data.moduleName);

                        commit('UPDATE_MODULE_ANSWER_LIST', response.data);
                    }
                    return Promise.resolve(response);
                },
                error => {
                    commit('UPDATE_MODULE_ANSWER_LIST', {});
                    return Promise.reject(error);
                }
            );
        },
    }
}
