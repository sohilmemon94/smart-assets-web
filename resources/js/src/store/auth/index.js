import { $themeBreakpoints } from '@themeConfig'
import AuthServices from '@/services/authServices';

export default {
  namespaced: true,
  state: {
    isLogin: localStorage.getItem("access_token") ? true : false,
    accessToken: localStorage.getItem("access_token"),
    authUserData: localStorage.getItem("userData") ? JSON.parse(localStorage.getItem("userData")) : [],
    authMyProfileData: {},
    userAvatar: (localStorage.getItem("userData") ? ((JSON.parse(localStorage.getItem("userData")).avatar != null) ? JSON.parse(localStorage.getItem("userData")).avatar :  '') : ''),
    //userAvatar: (localStorage.getItem("userData") ? ((JSON.parse(localStorage.getItem("userData")).avatar != null) ? JSON.parse(localStorage.getItem("userData")).avatar :  ((JSON.parse(localStorage.getItem("userData")).client_company_logo != null) ? JSON.parse(localStorage.getItem("userData")).client_company_logo :  "require('@/assets/images/avatars/13-small.png')")) : ''),
    userFullName: (localStorage.getItem("userData") ? JSON.parse(localStorage.getItem("userData")).fullName : ''),
    passwordFieldRequiredOld : '',
    passwordFieldRequiredNew : '',
    passwordFieldRequiredRetype : '',
    passwordFieldTypeOld: 'password',
    passwordFieldTypeNew: 'password',
    passwordFieldTypeRetype: 'password',
    isResetPasswordTokenValid: false,
    isSetPasswordTokenValid: false,
  },
  getters: {
    isLogin: state => {
      const { isLogin } = state
      return isLogin;
    },
    authUserData: state => {
      const { authUserData } = state
      return authUserData;
    },
    authMyProfileData: state => {
      const { authMyProfileData } = state
      return authMyProfileData;
    },
    userAvatar: state => {
      const { userAvatar } = state;
      return userAvatar;
    },
    userFullName: state => {
      const { userFullName } = state;
      return userFullName;
    },
    passwordFieldRequiredOld: state => {
      const { passwordFieldRequiredOld } = state
      return passwordFieldRequiredOld;
    },
    passwordFieldRequiredNew: state => {
      const { passwordFieldRequiredNew } = state
      return passwordFieldRequiredNew;
    },
    passwordFieldRequiredRetype: state => {
      const { passwordFieldRequiredRetype } = state
      return passwordFieldRequiredRetype;
    },
    passwordFieldTypeOld: state => {
      const { passwordFieldTypeOld } = state
      return passwordFieldTypeOld;
    },
    passwordFieldTypeNew: state => {
      const { passwordFieldTypeNew } = state
      return passwordFieldTypeNew;
    },
    passwordFieldTypeRetype: state => {
      const { passwordFieldTypeRetype } = state
      return passwordFieldTypeRetype;
    },
    isResetPasswordTokenValid: state => {
      const { isResetPasswordTokenValid } = state
      return isResetPasswordTokenValid;
    },
    isSetPasswordTokenValid: state => {
      const { isSetPasswordTokenValid } = state
      return isSetPasswordTokenValid;
    },
  },
  mutations: {
    UPDATE_IS_LOGIN(state, val) {
      state.isLogin = val
    },
    UPDATE_ACCESS_TOKEN(state, val) {
      state.accessToken = val
    },
    UPDATE_USER_DATA(state, val) {
      state.authUserData = val
    },
    UPDATE_MY_USER_PROFILE_DATA(state, val) {
      state.authMyProfileData = val
    },
    UPDATE_USER_AVATAR(state, val) {
      state.userAvatar = val
    },
    UPDATE_USER_FULL_NAME(state, val) {
      state.userFullName = val
    },
    UPDATE_PASSWORD_FIELD_REQUIRED_OLD(state, val) {
      state.passwordFieldRequiredOld = val
    },
    UPDATE_PASSWORD_FIELD_REQUIRED_NEW(state, val) {
      state.passwordFieldRequiredNew = val
    },
    UPDATE_PASSWORD_FIELD_REQUIRED_RETYPE(state, val) {
      state.passwordFieldRequiredRetype = val
    },
    UPDATE_PASSWORD_FIELD_TYPE_OLD(state, val) {
      state.passwordFieldTypeOld = val
    },
    UPDATE_PASSWORD_FIELD_TYPE_NEW(state, val) {
      state.passwordFieldTypeNew = val
    },
    UPDATE_PASSWORD_FIELD_TYPE_RETYPE(state, val) {
      state.passwordFieldTypeRetype = val
    },
    UPDATE_IS_RESET_PASSWORD_TOKEN_VALID(state, val) {
      state.isResetPasswordTokenValid = val
    },
    UPDATE_IS_SET_PASSWORD_TOKEN_VALID(state, val) {
      state.isSetPasswordTokenValid = val
    }
  },
  actions: {
    login({ commit, dispatch }, payload) {
      return AuthServices.login(payload).then(
          response => {
              if (response.status == true || response.status == 200) {
                  localStorage.setItem('access_token', response.data.access_token);
                  commit('UPDATE_IS_LOGIN', true);
                  commit('UPDATE_ACCESS_TOKEN', response.data.user.access_token);
                  commit('UPDATE_USER_DATA', response.data.user.loggedUserData);

                  //   Store the sites & subSites in local storage
                  localStorage.setItem('sites', JSON.stringify(response.data.sites));
                  localStorage.setItem('subSites', JSON.stringify(response.data.subSites));

                  commit('siteManager/UPDATE_SITE_OPTIONS', response.data.sites, { root: true });
                    commit('siteManager/UPDATE_SUB_SITE_OPTIONS', response.data.subSites, { root: true });

                  //commit('UPDATE_USER_ROLE', response.data.loggedUserData.role);
                  //commit('UPDATE_USER_PERMISSION', response.data.loggedUserData.permission);
                  //dispatch('common/clearError', null, { root: true });
              }
              return Promise.resolve(response);
          },
          error => {
              commit('UPDATE_IS_LOGIN', false);
              commit('UPDATE_ACCESS_TOKEN', null);
              commit('UPDATE_USER_DATA', {});
              //commit('UPDATE_USER_ROLE', '');
              //commit('UPDATE_USER_PERMISSION', {});

              return Promise.reject(error);
          }
      );
    },
    logout({ commit, dispatch }, payload) {
      return AuthServices.logout().then(
          response => {
              localStorage.removeItem('access_token');
              commit('UPDATE_IS_LOGIN', false);
              commit('UPDATE_ACCESS_TOKEN', null);
              commit('UPDATE_USER_DATA', {});
              commit('verticalMenu/RESET_SITE_MANAGER_DYNAMIC_MENU_DATA', null, { root: true });
              commit('verticalMenu/UPDATE_MENU_CHANGE', false, { root: true });
              //commit('UPDATE_USER_ROLE', '');
              //commit('UPDATE_USER_PERMISSION', {});

            //   Clearing the sites & subSites from local storage
                localStorage.removeItem('sites');
                localStorage.removeItem('subSites');
                localStorage.removeItem('defaultSite');
                localStorage.removeItem('defaultSubSite');

                // Clearing the dashboard related local storage variable
                localStorage.removeItem('clientId');
                localStorage.removeItem('locationId');
                localStorage.removeItem('areaId');
                localStorage.removeItem('areaArr');

                commit('siteManager/UPDATE_SITE_OPTIONS', [], { root: true });
                commit('siteManager/UPDATE_SUB_SITE_OPTIONS', [], { root: true });
                commit('siteManager/UPDATE_DEFAULT_SITE', null, { root: true });
                commit('siteManager/UPDATE_DEFAULT_SUB_SITE', null, { root: true });

            return Promise.resolve(response);
          },
          error => {
              localStorage.removeItem('access_token');
              commit('UPDATE_IS_LOGIN', false);
              commit('UPDATE_ACCESS_TOKEN', null);
              commit('UPDATE_USER_DATA', {});
              //commit('UPDATE_USER_ROLE', '');
              //commit('UPDATE_USER_PERMISSION', {});

              return Promise.reject(error);
          }
      );
    },
    findTokenPasswordReset({ commit }, payload) {
      return AuthServices.findTokenPasswordReset(payload).then(
          response => {
              if (response.status == true || response.status == 200) {
                  commit('UPDATE_IS_RESET_PASSWORD_TOKEN_VALID', true);
              }
              return Promise.resolve(response);
          },
          error => {
            commit('UPDATE_IS_RESET_PASSWORD_TOKEN_VALID', false);

            return Promise.reject(error);
          }
      );
    },
    resetPassword({ commit, dispatch }, payload) {
      return AuthServices.resetPassword(payload).then(
          response => {
              return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
      );
    },
    findTokenPasswordSet({ commit }, payload) {
      return AuthServices.findTokenPasswordReset(payload).then(
          response => {
              if (response.status == true || response.status == 200) {
                  commit('UPDATE_IS_SET_PASSWORD_TOKEN_VALID', true);
              }
              return Promise.resolve(response);
          },
          error => {
            commit('UPDATE_IS_SET_PASSWORD_TOKEN_VALID', false);

            return Promise.reject(error);
          }
      );
    },
    setPassword({ commit, dispatch }, payload) {
      return AuthServices.setPassword(payload).then(
          response => {
              return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
      );
    },

  },
}
