import UserPermissionServices from "@/services/userPermissionServices";

export default {
    namespaced: true,
    state: {
        companyAllUser: [],
        specificUserPermission: null,
    },
    getters: {
        companyAllUser(state) {
            const { companyAllUser } = state;
            return companyAllUser;
        },
        specificUserPermission(state) {
            const { specificUserPermission } = state;
            return specificUserPermission;
        },
    },
    mutations: {
        UPDATE_COMPANY_ALL_USER(state, payload) {
            state.companyAllUser = payload;
        },
        UPDATE_SPECIFIC_USER_PERMISSION(state, payload) {
            state.specificUserPermission = payload;
        },
    },
    actions: {
        // Get all user data from company in database
        getCompanyAllUser({ commit }) {
            return UserPermissionServices.getCompanyAllUser().then(
                (response) => {
                    if (response.status == true || response.status == 200) {
                        commit("UPDATE_COMPANY_ALL_USER", response.data);
                    }
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },

        // Save User wise Category/Module Permission data in database
        saveUserPermission({ commit, dispatch }, payload) {
            return UserPermissionServices.saveUserPermission(payload).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        // Get user permission data
        getUserPermission({ commit, dispatch }, payload) {
            return UserPermissionServices.getUserPermission(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        commit("UPDATE_SPECIFIC_USER_PERMISSION", response.data);
                    }

                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
    },
};
