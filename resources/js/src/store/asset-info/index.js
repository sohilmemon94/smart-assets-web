import AssetInfoServices from '@/services/assetinfoServices';

export default {
    namespaced: true,
    state:{
        asset_nfc_uuid: ""
    },
    actions: {

        // Store site data
        submitAssetReport({ commit }, payload, config) {
            return AssetInfoServices.submitAssetReport(payload, config).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            )
        },

        // Get specific Asset data
        getAssetInfo({ commit }, payload, config) {
            return AssetInfoServices.getAssetInfo(payload, config).then(
            response => {
                //console.log(response);
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },
        },
}
