import ModuleServices from '../../services/moduleServices';

export default {
  namespaced: true,
  state: {
    moduleList: {
      modules: {
        data: [],
        to: 0,
        from: 0
      }
    },
    categoryName: '',
    moduleData: {
      module_id: '',
      module_category_id: '',
      module_template_id: '',
      module_name: '',
    //   module_prefix: '',
      module_desc: '',
      module_icon: [],
      module_is_default: '',
      module_has_activity: '',
      module_has_image: true,
      module_allow_duplicates: '',
      module_status: true,
    },
    modalButtonName: 'Add',
    isCategoryDisplay: true,
    editMode: false,
    viewMode: false,
  },
  mutations: {
    UPDATE_CATEGORY_NAME(state, payload) {
      state.categoryName = payload;
    },
    UPDATE_MODULE_LIST(state, payload) {
      state.moduleList = payload;
    },
    UPDATE_MODULE_DATA(state, payload) {
      state.moduleData = payload;
    },
    UPDATE_MODAL_BUTTON_NAME(state, val) {
      state.modalButtonName = val
    },
    UPDATE_IS_CATEGORY_DISPLAY(state, val) {
      state.isCategoryDisplay = val
    },
    UPDATE_EDIT_MODE(state, val) {
      state.editMode = val
    },
    UPDATE_VIEW_MODE(state, val) {
      state.viewMode = val
    },
  },
  getters: {
    categoryName(state) {
      return state.categoryName;
    },
    moduleList: state => {
      const { moduleList } = state
      return moduleList;
    },
    moduleData: state => {
      const { moduleData } = state
      return moduleData;
    },
    modalButtonName: state => {
      const { modalButtonName } = state
      return modalButtonName;
    },
    isCategoryDisplay: state => {
      const { isCategoryDisplay } = state
      return isCategoryDisplay;
    },
    editMode: state => {
      const { editMode } = state
      return editMode;
    },
    viewMode: state => {
      const { viewMode } = state
      return viewMode;
    },
  },
  actions: {
    // Get all field based on template id
    getModuleList({ commit }, payload) {
      return ModuleServices.getAllModule(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CATEGORY_NAME', response.data.categoryName)
            commit('UPDATE_MODULE_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_MODULE_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get all Module data in database
    getAllModule({ commit }, payload) {
      return ModuleServices.getAllModule(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_MODULE_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_MODULE_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Store Module data in database
    saveModule({ commit }, data) {
      return ModuleServices.saveModule(data.payload, data.config).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    // Get specific module data
    getModuleData({ commit }, payload) {
      return ModuleServices.getModuleData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            response.data.module_has_activity = (response.data.module_has_activity == 'Y') ? true : false;
            response.data.module_has_image = (response.data.module_has_image == 'Y') ? true : false;
            response.data.module_allow_duplicates = (response.data.module_allow_duplicates == 'Y') ? true : false;
            response.data.module_status = (response.data.module_status == 'Y') ? true : false;

            // For inspection schedule rgby
            if(response.data.module_inspection_schedule_rgby == 'Y') {
              response.data.module_inspection_schedule = 'RGBY';
            }

            commit('UPDATE_MODULE_DATA', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // update module field data in database
    updateModuleData({ commit, dispatch }, data) {
      return ModuleServices.updateModuleData(data.id, data.payload, data.config).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // update module icon data in database
    uploadModuleIcon({ commit, dispatch }, data) {
      return ModuleServices.uploadModuleIcon(data.payload, data.config).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Remove module icon data in database
    removeModuleIcon({ commit, dispatch }, payload) {
      return ModuleServices.removeModuleIcon({ module_id: payload }).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // update module data in database
    updateModule({ commit, dispatch }, temp) {
      let payload = temp.payload;
      let config = temp.config;
      let id = temp.id;
      return ModuleServices.updateModuleData(id, payload, config).then(
        response => {
          if (response.status == true || response.status == 200) {

          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Change Module Status in database
    changeModuleStatus({ commit, dispatch }, payload) {
      return ModuleServices.changeModuleStatus(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Delete Module in database
    deleteModule({ commit, dispatch }, payload) {
      return ModuleServices.deleteModuleData(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },


  },

}
