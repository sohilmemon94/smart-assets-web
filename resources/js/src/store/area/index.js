import SubSiteServices from '../../services/subsiteServices';


export default {
    namespaced: true,
    state: {
        subSiteList: {
            subsites: {
                data: [],
                to: 0,
                from: 0
            }
        },
        subSiteData: {
            sub_site_name: '',
            sub_site_uuid: '',
            sub_site_description: '',
            sub_site_status: true,
            created_by: '',
            site_id: '',
            user_id: ''
          },

        subSiteButtonName: 'Add',
        editMode: false,

    },
    mutations: {
        UPDATE_SUB_SITE_BUTTON_NAME(state, val) {
            state.subSiteButtonName = val
          },
          UPDATE_SUB_SITE_DATA(state, payload) {
            state.subSiteData = payload;
          },
          UPDATE_EDIT_MODE(state, val) {
            state.editMode = val
          },
        UPDATE_SUB_SITE_LIST(state, val) {
            state.subSiteList = val
        }
    },
    actions: {
        // Get all subSite data in database
        getAllSubSite({ commit }, payload) {
            return SubSiteServices.getAllSubSite(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        commit('UPDATE_SUB_SITE_LIST', response.data);
                        commit('UPDATE_SUB_SITE_DATA', response.data);
                    }
                    return Promise.resolve(response);
                },
                error => {
                    commit('UPDATE_SUB_SITE_LIST', {});
                    return Promise.reject(error);
                }
            );
        },
        // Store subSite data in database
        saveSubSite({ commit }, payload) {
            return SubSiteServices.storeSubSite(payload).then(
                response => {

                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            )
        },
        // Get specific subSite data
        getSubSiteData({ commit }, payload) {
            return SubSiteServices.getSubSiteData(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        response.data.sub_site_status = (response.data.sub_site_status == 'Y') ? true : false;

                        commit('UPDATE_SUB_SITE_DATA', response.data);
                      }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        // update subSite data in database
        updateSubSite({ commit, dispatch }, payload) {
            return SubSiteServices.updateSubSiteData(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {

                    }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        // Change subSite Status in database
        changeSubSiteStatus({ commit, dispatch }, payload) {
            return SubSiteServices.changeSubSiteStatus(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },
           // Delete subSite in database
        deleteSubSite({ commit, dispatch }, payload) {
            return SubSiteServices.deleteSubSiteData(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },

        // Get Site in database
        getSiteOnSubSite({ commit, dispatch }, payload) {
            return SubSiteServices.getSiteOnSubSite(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },

        // Get Site in database
        getUserOnSubSite({ commit, dispatch }, payload) {
            return SubSiteServices.getUserOnSubSite(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },



    },
    getters: {
        subSiteButtonName: state => {
            const { subSiteButtonName } = state
            return subSiteButtonName;
          },
          subSiteData: state => {
            const { subSiteData } = state
            return subSiteData;
          },
          editMode: state => {
            const { editMode } = state
            return editMode;
          },
        subSiteList: state => {
            const { subSiteList } = state
            return subSiteList;
        }
    },

}
