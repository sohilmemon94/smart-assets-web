import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import auth from './auth'
import user from './user'
import location from './location'
import area from './area'
import category from './category'
import template from './template'
import templateField from './template-field'
import module from './module'
import moduleField from './module-field'
import moduleQuestion from './module-question'
import moduleAnswer from './module-answer'
import categoryModule from './category-module'
import siteManager from './site-manager'
import assets from './site-manager/assets'
import moveAssets from './site-manager/moveAsset'
import activity from './site-manager/activity'
import documents from './site-manager/documents'
import areaUserPermission from './area-user-permission'
import inspection from './site-manager/inspection'
import gotoSite from './area/gotosite'
import dashboard from './dashboard'
import dashboardListing from './dashboard/listing'
import assetDetail from './dashboard/assetDetail'
import assetInfo from './asset-info'
import dashboardListPrint from './dashboard/listPrint'
import dashboardDetailPrint from './dashboard/detailPrint'
import confinedSpace from './confined-space'
import company from './company'
import userPermission from './user-permission'




Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        app,
        appConfig,
        verticalMenu,
        auth,
        user,
        location,
        area,
        category,
        template,
        templateField,
        module,
        moduleField,
        moduleQuestion,
        categoryModule,
        siteManager,
        assets,
        activity,
        documents,
        areaUserPermission,
        inspection,
        gotoSite,
        dashboard,
        dashboardListing,
        assetDetail,
        assetInfo,
        dashboardListPrint,
        dashboardDetailPrint,
        moduleAnswer,
        moveAssets,
        confinedSpace,
        company,
        userPermission,
    },
    strict: process.env.DEV,
})
