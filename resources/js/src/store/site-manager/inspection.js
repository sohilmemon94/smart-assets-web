import siteManagerServices from '../../services/siteManagerServices.js';


export default {
    namespaced: true,
    state: {
        //inspection: [],
        inspectionList: {
            inspection: {
              data: [],
              to: 0,
              from: 0
            }
          },

        selectedAssetId: null,
        table: {
            asset_id: null,
            sortBy: 'inspection_id',
            sortDesc: true,
            perPage: 25,
            search: null,
            page: 1,
        }
    },
    mutations: {
        UPDATE_INSPECTION_LIST(state, payload) {
            state.inspectionList = payload;
        },
        UPDATE_TABLE(state, payload) {
            state.table = payload;
        },
        UPDATE_SELECTED_ASSET_ID(state, payload) {
            state.selectedAssetId = payload;
        },
    },
    actions: {
        getAssetInspection({ commit, state }, payload) {
            if(payload) {
                state.table.asset_id = payload;
            }
            return new Promise((resolve, reject) => {
                siteManagerServices.getAssetInspectionList(state.table).then((response) => {
                    // commit('UPDATE_TABLE', payload);
                    commit('UPDATE_INSPECTION_LIST', response.data);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        deleteBatchInspections({ dispatch, state }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.deleteBatchInspections(payload).then((response) => {

                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
    },
    getters: {
        inspectionList: state => {
            const { inspectionList } = state;
            return inspectionList;
        },
        table: state => {
            const { table } = state;
            return table;
        },
        selectedAssetId(state) {
            return state.selectedAssetId;
        },
    },
}
