import Http from '@/libs/axios';

class AppServices {

     // Get Client Panel Site in system via API call
     async getAllError(filleter) {
        return await Http.get('/error-log', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get latest version of system via API call
    async getLatestVersion() {
        return await Http.get('/latest-version').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Store latest version of system via API call
    async saveSystemVersion(payload) {
        return await Http.post('/save-system-version', payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }
    
}

export default new AppServices();
