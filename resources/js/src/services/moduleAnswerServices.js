import Http from '@/libs/axios';

class ModuleAnswerServices {

    // Get Client Panel in system via API call
    async getModuleAnswerList(filleter) {
        return await Http.get('moduleAnswer', {

            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }
}

export default new ModuleAnswerServices();

