import Http from '@/libs/axios';

class ConfinedSpaceServices {

    // Get Client Panel Site in system via API call
    async getAllConfinedSpace(filleter) {
        return await Http.get('/confinedSpace', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get Client Confined Space Data in system via API call
    async getConfinedSpaceData(payload) {
        return await Http.get(`confinedSpace/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

}

export default new ConfinedSpaceServices();
