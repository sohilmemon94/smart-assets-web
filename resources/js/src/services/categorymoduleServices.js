import Http from '@/libs/axios';

class categoryModuleServices {

    // Get Client Panel Category/Module in system via API call
    async getAllCategoryModule(filleter) {
    //    console.warn(filleter);
        // console.log(filleter.id);
        return await Http.get(`/categoryModule`, {
            params: filleter
        }).then((response) => {
         //   console.log(response);
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get Client Panel Site in system via API call
    async saveCategoryModulePermission(payload) {
        return await Http.post(`changeSubSitePermission/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }




}


export default new categoryModuleServices();
