import Http from '@/libs/axios';

class SiteServices {

    // Get Client Panel Site in system via API call
    async getAllSite(filleter) {
        return await Http.get('/site', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Client Panel Site in system via API call
    async saveSite(payload) {
        return await Http
            .post('/site', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Delete Client Panel Site API call
    async deleteSiteData(payload) {
        return await Http.post(`/deleteSite`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Get Client Panel Site in system via API call
    async getSiteData(payload) {
        return await Http.get(`site/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Client Panel Site in system via API call
    async updateSiteData(payload) {
        return await Http.put(`site/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Site status API call
    async changeSiteStatus(payload) {
        return await Http.post(`changeSiteStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

}

export default new SiteServices();
