<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $head }}</title>
    <style>
        @page {
            margin: 10px;
            size: A4;
            /*or width x height 150mm 50mm*/
        }

        body {
            margin: 24px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin: 0 auto;
            font-family: Arial, Helvetica, sans-serif;
        }

        table tr td,
        table tr th {
            padding: 0px 8px;
            margin: 0;
            box-sizing: border-box;
        }

        table.data-table tr td {
            padding: 0px 12px;
            margin: 0;
            box-sizing: border-box;
        }

        table,
        th,
        td {
            border: 1px solid #f1f2f3;
        }

        .report-brand,
        .report-brand li {
            padding: 0;
            margin: 0;
            /* margin-top: 12px; */
        }

        .report-brand li {
            display: inline-block;
            vertical-align: middle;
        }

        .report-brand-name {
            font-size: 32px;
            padding-left: 10px;
            color: #005abe;
        }

        @media(max-width:767px) {
            table {
                width: 100%;
                overflow-x: auto;
            }
        }
    </style>
</head>

<body>
    <table style="border-bottom:none !important;">
        <thead>
            <tr style="">
                <td style="border:none; padding-top: 5px; width:50%">
                    <h3 style="margin:0px;">{{ $head }}</h3>
                </td>
                <td rowspan="2" style="border:none; text-align:right; width:50%">
                    <ul class="report-brand">
                        <li><img src="{{ url('pdf_image/logo/sa_logo.png') }}" width="60px"></li>
                    </ul>
                </td>
            </tr>
            <tr style="">
            </tr>
        </thead>
    </table>
    <table style="font-size: 12px;" class="data-table">
        <thead style="background:#cccccc;">
            <tr>
                <th colspan="2">Error Log Details</th>
            </tr>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Name</th>
                <td>{{ $el_name }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $el_description }}</td>
            </tr>
            <tr>
                <th>Type</th>
                <td>{{ $el_type }}</td>
            </tr>
            <tr>
                <th>Raised</th>
                <td>{{ $el_raised }}</td>
            </tr>
        </tbody>
    </table>
</body>

</html>
