# Smart Asset

## Introduction

The Smart Asset project is a comprehensive asset management system that enables users to efficiently manage, inspect, and analyze assets through a user-friendly dashboard. The project incorporates three distinct roles to provide varying levels of access and functionality: Super Admin, Client, and User.

The Super Admin role has the highest level of authority and is responsible for overseeing the entire system. They have the ability to manage user accounts, configure settings, and customize the system according to specific requirements. The Super Admin also has access to advanced analytics and reporting features.

The Client role is designed for organizations or individuals who own or manage assets. Clients have the ability to create and manage multiple user accounts within their organization, defining roles and permissions for each user. They can add, modify, or remove assets, as well as schedule inspections and maintenance activities. Clients can generate reports and receive notifications about asset-related activities.

The User role is intended for individuals who interact with the assets but do not require administrative privileges. Users can view asset information, submit inspection reports, and access relevant documentation. They can also request maintenance or repairs and track the progress of their requests.

The system provides a centralized dashboard that allows users to access asset information, such as location, condition, maintenance history, and any associated documents. The dashboard also provides visualizations and analytics, enabling users to gain insights into asset performance, utilization, and maintenance trends.

Overall, the Smart Asset project aims to streamline asset management processes, enhance efficiency, and improve decision-making through a user-friendly interface and comprehensive features for managing, inspecting, and analyzing assets.

## System Requirements

| Requirement     | Version/Condition       |
|-----------------|-------------------------|
| PHP             | v8.0.2 or Above         |
| MySQL           | 5.7+                    |
| Composer        | v2.0.4 or Above         |
| Node            | LTS version              |
| NPM             | 8.11.0 or Above         |

## Installation

```sh
git clone https://prashant_tridhya@bitbucket.org/rmtadmin01/web-portal-project.git
git branch
git checkout master
git pull origin master
composer install
php artisan migrate
php artisan db:seed
php artisan passport:install
php artisan key:generate
npm install / npm install --legacy-peer-deps
php artisan optimize
npm run prod
```

## Usage

- After successfully running the project, you will see a login screen. Here are the default user credentials for different roles:


| **Role**       | **Email**              | **Password**     |
|----------------|------------------------|------------------|
| Super Admin    | superadmin@yopmail.com | superadmin@123   |
| Client Admin   | clientadmin@yopmail.com| clientadmin@123       |
| User           | sitemanager@yopmail.com| sitemanager@123  |

- We have also created an API for the mobile application. You can find the API documentation [here](https://docs.google.com/spreadsheets/u/1/d/1cjL-6KfNpPkt5a9De8XvDCAe0n72MS98fJ34IH592ps/edit#gid=709254322).

## Features

Based on the role features define as below.
    
- Super Admin Panel -

    - 2FA and recaptcha for super user login functionality.

    - As a Super Admin, you can access the analytics dashboard to view analytics for all assets. You can also create multiple Super Admin users to perform the same tasks.

    - To manage clients, you can create client entities that represent companies or organizations. For each client, you can create a Client Admin user who can access the client panel.

    - Within the system, you have the ability to create default categories, modules, module fields, and templates to customize the data structure and presentation of your assets.

    - Make sure to regularly check the system for any generated errors. These errors will provide valuable insights into potential issues and help us ensure the system's stability and performance.

- Client Admin - 

    - Recaptcha functionality in login page.

    - As a Client Admin, you can access the analytics dashboard to view analytics for all assets to related company. You can also create users to use web panel and mobile application.

    - Within the system, you have the ability to create and manage various components:

        - **Location**: Create locations to organize assets based on geographical locations.

        - **Area**: Define specific areas within each location for further categorization.

        - **Category**: Create categories to classify assets based on their characteristics or purpose.

        - **Module**: Define modules to represent different functional modules or subsystems.

        - **Module Fields**: Customize module fields to capture specific information related to each module/Asset. Here we created dynamic fields.

        - **Templates**: Create templates to define standardized structures or layouts for asset data.

    - Set user permission based on user access like location, area, category and modules.

- User - 

    - Recaptcha functionality in login page.

    - **Add Asset**: Users can add new assets to the system. They can provide necessary details such as asset name, description, location, category, and other relevant information.

    - **View All Assets**: Users have access to view assets as per permission. This allows them to search, filter, and sort assets based on their preferences.

    - **Move Asset Functionality**: Users can move assets from one location to another within the system. This provides flexibility in managing assets based on changes in physical locations or organizational requirements.

    - **Asset Details**: Users can view detailed information about each asset, including inspection history, asset documents, and asset activity logs. This enables users to track the inspection status, access relevant documents, and review the activity associated with each asset.

    - User can use mobile application using user credential.


