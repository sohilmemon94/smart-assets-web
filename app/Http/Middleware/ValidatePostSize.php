<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Exceptions\PostTooLargeException;

class ValidatePostSize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Http\Exceptions\PostTooLargeException
     */
    public function handle($request, Closure $next)
    {
        $max = $this->file_upload_max_size();

        if ($max > 0 && $request->server('CONTENT_LENGTH') > $max) {
            $maxSize = $max/1024/1024;   // convert to MB
            return response()->json([
                'status'  => false,
                'message' => null,
                'errors'  => [
                    'file' => 'The file size must not be greater than '.$maxSize.' mb.'
                ]
            ], 413);
            // throw new PostTooLargeException;
        }

        return $next($request);
    }

    /**
     * Determine the server 'post_max_size' as bytes.
     *
     * @return int
     */
    protected function getPostMaxSize()
    {
        if (is_numeric($postMaxSize = ini_get('post_max_size'))) {
            return (int) $postMaxSize;
        }

        $metric = strtoupper(substr($postMaxSize, -1));
        $postMaxSize = (int) $postMaxSize;

        return match ($metric) {
            'K' => $postMaxSize * 1024,
            'M' => $postMaxSize * 1048576,
            'G' => $postMaxSize * 1073741824,
            default => $postMaxSize,
        };
    }

    protected function file_upload_max_size() {
        $max_size = -1;
        if ($max_size < 0) {
          // Start with post_max_size.
          $post_max_size = $this->parse_size(ini_get('post_max_size'));
          if ($post_max_size > 0) {
            $max_size = $post_max_size;
          }

          // If upload_max_size is less, then reduce. Except if upload_max_size is
          // zero, which indicates no limit.
          $upload_max = $this->parse_size(ini_get('upload_max_filesize'));
          if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
          }
        }
        return $max_size;
    }

    protected function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
          // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
          return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
          return round($size);
        }
    }
}
