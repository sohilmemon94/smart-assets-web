<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Models\Version;

class VersionController extends Controller
{
    use GeneralTrait;

    /**
	 * Get latest version of system
	 */
	public function getLatestVersion()
	{
		try {
			$latestVersion = Version::latest()->first('version');

            if(empty($latestVersion)) {
                $latestVersion = '1.0.0';
            }

			return $this->returnSuccessMessage(null, $latestVersion);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
	 * Set latest version of system
	 */
    public function setLatestVersion(Request $request) {
        try {
            if($request->version) {
                Version::create(
                    ['version'    => $request->version,
                    'created_by' => auth()->user()->id]
                );
            }
            
            return $this->returnSuccessMessage(null, []);
        } catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
    }
}
