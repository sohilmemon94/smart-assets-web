<?php

namespace App\Http\Controllers\API;

use Throwable;
use App\Models\Site;
use App\Models\User;
use App\Models\Module;
use App\Models\SubSite;
use App\Models\SubSiteUser;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Models\SubSitePermission;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\SubSiteUserPermission;
use Illuminate\Support\Facades\Validator;

class SubSiteController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $perPage  = isset($request->perPage) ? $request->perPage : null;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'sub_site_id';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status = $request->status;
            if($request->site_id && $perPage){
                $subsiteData = SubSite::with('site')->where('sub_site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id)->where('site_id', $request->site_id);
            }else{
                $subsiteData = SubSite::with('site')->where('sub_site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id);
            }


            // Status filter
            if ($status) {
                $subsiteData = $subsiteData->where('sub_site_status', $status);
            }

            // Search filter
            if ($search) {
                $subsiteData = $subsiteData->where(function ($query) use ($search) {
                    $query->orWhere('sub_site_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('sub_site_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('sub_site_description', 'LIKE', '%' . $search . '%')
                        ->orWhereHas('site', function ($siteQuery) use ($search) {
                            $siteQuery->where('site_name', 'LIKE', '%' . $search . '%');
                        });
                });
            }

        if ($perPage) {
            if($sortBy == 'site.site_name'){
                $sortBy = 'site_id';
            }
            $subsiteData = $subsiteData->orderBy($sortBy, $sortDesc)->paginate($perPage);

            $pagination = [
                "total"        => $subsiteData->total(),
                "current_page" => $subsiteData->currentPage(),
                "last_page"    => $subsiteData->lastPage(),
                "from"         => $subsiteData->firstItem(),
                "to"           => $subsiteData->lastItem()
            ];

            $data = ['subsites' => $subsiteData, "total" => $subsiteData->total(), 'pagination' => $pagination];
        } else {
            //echo $id;
            if($request->site_id){
                $subsiteData = SubSite::where('sub_site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id)->where('site_id', $request->site_id)->get();
                    $subSiteList = [] ;
                    foreach($subsiteData as $key=>$value) {

                       $subSiteList[]=  Array (
                        "value" => $value['sub_site_id'],
                        "text" => $value['sub_site_name']
                     );

                    }
            }else{
                $subsiteData = SubSite::where('sub_site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id)->get();
            }

            $data = ['subsites' => $subsiteData, 'subSiteList' => $subSiteList];
        }

            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'site_id' => 'required',
                'sub_site_name' => 'required|string',
            ]);
            if ($validator->fails()) {
                // get all errors as single string
                return $this->returnValidation($validator->errors());
            }
            $subsiteData = array(
                'site_id' => $request->site_id,
                'sub_site_name' => $request->sub_site_name,
                'sub_site_uuid' => $request->sub_site_uuid ?? NULL,
                'sub_site_description' => $request->sub_site_description ?? NULL,
                'sub_site_status' => ($request->sub_site_status == false) ? 'N': 'Y',
                'created_by'  => Auth::user()->id,
                'company_id'       => Auth::user()->company_id,
            );
            // Save data
            $subSiteObj = SubSite::create($subsiteData);
            $this->subSiteUser($request->user_id, $subSiteObj->sub_site_id, $subSiteObj->site_id);
            $this->defaultPermission($subSiteObj->sub_site_id);
            $this->defaultWithUserPermission($subSiteObj->sub_site_id, $request->user_id);



            return $this->returnSuccessMessage('Area Created Successfully.');
        }
        catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $subsite = SubSite::with('site')->with('subSiteUser')->where('sub_site_id', $id)->first();
            $subsiteData = $subsite->setRelation('subSiteUser', $subsite->subSiteUser->keyBy('ssu_user_id'));
            if($subsiteData){
            $subsiteData = $subsiteData->toArray();

            $subsiteData = array_keys($subsiteData['sub_site_user']);
            $selected = [];

            foreach($subsiteData as $key => $value){
                $userData = User::whereNotNull('first_name')->where('id', $value)->where('company_id', Auth::user()->company_id)->first();
                $selected[] = Array (
                    "value" => $userData->id,
                    "text" => $userData->first_name.' '. $userData->last_name
                );
            }
            $subsite['user_id'] = $selected;
            }


            if ($subsite) {
                return $this->returnSuccessMessage('Area retrieved successfully.', $subsite);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{

                $validator = Validator::make($request->all(), [
                    'site_id' => 'required',
                    'sub_site_name' => 'required|string',
                    'user_id' => 'required',
                    //'sub_site_uuid' => 'required|unique:sub_sites,sub_site_uuid,' . $id . ',sub_site_id',
                ]);
                if ($validator->fails()) {
                    // get all errors as single string
                    return $this->returnValidation($validator->errors());
                }

                $subsiteData = array(
                    'site_id' => $request->site_id,
                    'sub_site_name' => $request->sub_site_name,
                    'sub_site_uuid' => $request->sub_site_uuid ?? NULL,
                    'sub_site_description' => $request->sub_site_description ?? NULL,
                    'sub_site_status' => ($request->sub_site_status == false) ? 'N': 'Y',
                    'updated_by'  => Auth::user()->id,
                );

                // Save data
                SubSite::where('sub_site_id', $id)->update($subsiteData);

                SubSiteUser::where('ssu_sub_site_id', $id)->delete();
                $this->subSiteUser($request->user_id, $id, $request->site_id);

                return $this->returnSuccessMessage('Area Updated Successfully.');

        }
        catch (\Exception $e) {
                return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }


    }

    /** Sub Site User Added exiting Sub site user permission */
    public function updateSubSiteUser(Request $request, $id){
        if($id){
            $subsite = SubSite::where('sub_site_id', $id)->where('company_id', Auth::user()->company_id)->first();
            if($subsite->site_id){
                $users = [];
                $users[] = array(
                    "value" => $request->user_id
                );
                $this->subSiteUser($users, $id, $subsite->site_id);
                $this->defaultWithUserPermission($id, $users);
                return $this->returnSuccessMessage('Area Updated Successfully.');
            }
        }

    }

    /** Sub Site Users Manage */
    public function subSiteUser($users, $id, $site_id){

         if($id && $users && $site_id){
                foreach($users as  $obj){
                    $subSiteUsers = array(
                        'ssu_site_id' => $site_id,
                        'ssu_sub_site_id' => $id,
                        'ssu_user_id' => $obj['value'],
                        'created_by'  => Auth::user()->id,
                        'company_id'       => Auth::user()->company_id,
                    );

                    SubSiteUser::create($subSiteUsers);
                }

            }
    }

    /** Sub-Site Category/Module Permission */
    public function subSitePermission(Request $request, $id){
        $keys_array = array_map(function ($item){
            if (isset($item['module_category_id'])){
                return $item['module_category_id'].'_'.$item['id'];
            } else {
                return 'null';
            }
        }, $request->data);

        $oldRecord = SubSitePermission::where('ssp_sub_site_id', $id)
                        ->selectRaw('ssp_category_id,ssp_module_id,CONCAT(ssp_category_id, "_", ssp_module_id) as `key`')
                        ->whereNotIn(DB::raw('CONCAT(ssp_category_id, "_", ssp_module_id)'), $keys_array)
                        ->pluck('key')
                        ->toArray();
                        
        $exist_category = SubSiteUserPermission::where('ssup_sub_site_id', $id)
                            ->whereIn(DB::raw('CONCAT(ssup_category_id, "_", ssup_module_id)'), $oldRecord)
                            ->get();

        if(sizeOf($exist_category) > 0) {
            // Removed all permission like select all then get blank in data
            if($id > 0 && empty($request->data)) {
                // Remove area permission
                SubSitePermission::where('ssp_sub_site_id', $id)->where('company_id', Auth::user()->company_id)->delete();

                // Remove user area permission
                SubSiteUserPermission::where('ssup_sub_site_id', $id)->where('company_id', Auth::user()->company_id)->delete();
            } else {
                $existCategoryData = $exist_category->toArray();
                
                foreach($existCategoryData as $existCategory) {
                    // Remove area permission
                    SubSitePermission::where('ssp_sub_site_id', $id)->where('ssp_category_id', $existCategory['ssup_category_id'])->where('ssp_module_id', $existCategory['ssup_module_id'])->where('company_id', Auth::user()->company_id)->delete();
                    
                    // Remove user area permission
                    SubSiteUserPermission::where('ssup_id', $existCategory['ssup_id'])->delete();
                }
            }

            return $this->returnSuccessMessage('Category/Module Permission Removed Successfully.');
            // return $this->returnError(405, 'Opps! Not allow to change permission, as this Category/Module already assigned to other users.');
        }else{
            SubSitePermission::where('ssp_sub_site_id', $id)->delete();

            if($id > 0 && $request->data){
                foreach($request->data as  $obj){
                    if(isset($obj['module_category_id'])){
                        $subSitePermission = array(
                            'ssp_sub_site_id' => $id,
                            'ssp_category_id' => $obj['module_category_id'] ?? $obj['id'],
                            'ssp_module_id'   => $obj['id'],
                            'created_by'      => Auth::user()->id,
                            'company_id'      => Auth::user()->company_id,
                        );

                        SubSitePermission::create($subSitePermission);
                    }
                }
            }
            return $this->returnSuccessMessage('Category/Module Permission Created Successfully.');
        } ;


    }

    /**
     * Sub site user wise category/module permission store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userPermission(Request $request, $id){
        SubSiteUserPermission::where('ssup_sub_site_id', $id)->where('ssup_user_id', $request->user_id)->delete();
        if($id && $request->data && $request->user_id){
            foreach($request->data as  $obj){
                if(isset($obj['module_category_id'])){
                    $subSitePermission = array(
                        'ssup_sub_site_id' => $id,
                        'ssup_user_id' => $request->user_id,
                        'ssup_category_id' => $obj['module_category_id'] ?? $obj['id'],
                        'ssup_module_id' => $obj['id'],
                        'created_by'  => Auth::user()->id,
                        'company_id'       => Auth::user()->company_id,
                    );

                    SubSiteUserPermission::create($subSitePermission);
                }
            }
        }
        return $this->returnSuccessMessage('User Permission Created Successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUserPermission(Request $request, $id)
    {

        if ($request->user_id && $id) {
            $exit_record = SubSiteUserPermission::where('ssup_user_id', $request->user_id)->where('ssup_sub_site_id', $id)->where('company_id', Auth::user()->company_id)->exists();
            if($exit_record){
                return $this->returnError(405, 'Opps! Not allow to Delete User, as this Category/Module already assigned to this user.');
            }else{
                SubSiteUser::where('ssu_user_id', $request->user_id)->where('ssu_sub_site_id', $id)->where('company_id', Auth::user()->company_id)->delete();

               return $this->returnSuccessMessage('Users Permissions User deleted successfully.');
            }
        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * area create after id set all category/module permission default.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function defaultPermission($id){

        if($id){
            $moduleData = Module::where('module_user_id', Auth::user()->id)->get(['module_id', 'module_category_id']);
            if($id > 0 && $moduleData){
                foreach($moduleData as  $obj){
                    if(isset($obj['module_category_id'])){
                        $subSitePermission = array(
                            'ssp_sub_site_id' => $id,
                            'ssp_category_id' => $obj['module_category_id'],
                            'ssp_module_id' => $obj['module_id'],
                            'created_by'  => Auth::user()->id,
                            'company_id'       => Auth::user()->company_id,
                        );

                        SubSitePermission::create($subSitePermission);
                    }
                }
            }
        }

    }

    /**
     * area create after id set all category/module permission user wise default.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function defaultWithUserPermission($id, $userArray){

        if($id){
            $moduleData = Module::where('module_user_id', Auth::user()->id)->get(['module_id', 'module_category_id']);

            if($userArray && $id > 0 && $moduleData){
                foreach($userArray as $userObj){
                    foreach($moduleData as  $obj){
                        if(isset($obj['module_category_id'])){
                            $subSitePermission = array(
                                'ssup_sub_site_id' => $id,
                                'ssup_user_id' => $userObj['value'],
                                'ssup_category_id' => $obj['module_category_id'] ,
                                'ssup_module_id' => $obj['module_id'],
                                'created_by'  => Auth::user()->id,
                                'company_id'       => Auth::user()->company_id,
                            );

                            SubSiteUserPermission::create($subSitePermission);
                        }
                    }
                }
            }
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id) {

            $exist_area = SubSitePermission::where('ssp_sub_site_id', $request->id)->where('company_id', Auth::user()->company_id) -> exists();

            if($exist_area){
                return $this->returnError(405, 'Opps! Not allow to Delete, as this Area already assigned to category/module permission.');
            }else{
                $subsiteData = SubSite::where('sub_site_id', $request->id)->first();
                SubSiteUser::where('ssu_sub_site_id', $request->id)->delete();
                $subsiteData->delete();
                return $this->returnSuccessMessage('Area deleted successfully.', $subsiteData);
            }

        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
	 * Change Site status data
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function changeSubSiteStatus(Request $request)
	{
		try {
			if ($request->id) {
				if ($request->status == 'Y') {
					SubSite::where('sub_site_id', $request->id)->update(['sub_site_status' => 'N']);
				} else {
					SubSite::where('sub_site_id', $request->id)->update(['sub_site_status' => 'Y']);
				}

				return $this->returnSuccessMessage('You have successfully changed Area status.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
    }

    public function getSiteOnSubSite(Request $request){

        $site_id = SubSite::where('sub_site_id', $request->id)->first('site_id');

        return $site_id;

    }

    public function getUserOnSubSite(Request $request) {
        return ['ssu_user_id' => Auth::id()];
        
        $subSiteUserData = SubSiteUser::select('ssu_user_id')->where('ssu_site_id', $request->site_id)->where('ssu_sub_site_id', $request->sub_site_id)->get()->toArray();

        $userIdArray = array_column($subSiteUserData, 'ssu_user_id');

        if(in_array(Auth::id(), $userIdArray)) {
            return ['ssu_user_id' => Auth::id()];
        } else {
            return ['ssu_user_id' => sizeOf($subSiteUserData) > 0 ? $subSiteUserData[0]['ssu_user_id'] : ''];
        }
    }
}
