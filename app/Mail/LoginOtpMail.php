<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LoginOtpMail extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $toEmail, $otp, $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otp, $name)
    {
        $this->otp     = $otp;
        $this->name    = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.loginOtpMail')
            ->subject("Login OTP")
            ->from(config('setting.SUPPORT_EMAIL'))
            ->with([
                'otp'     => $this->otp,
                'name'    => $this->name
            ]);
    }
}
