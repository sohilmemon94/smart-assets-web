<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubSitePermission extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'ssp_id';
    protected $table      = 'sub_site_permission';

    protected $fillable = [
        'ssp_sub_site_id', 'ssp_category_id', 'ssp_module_id', 'company_id', 'created_by', 'updated_by'
    ];




}
