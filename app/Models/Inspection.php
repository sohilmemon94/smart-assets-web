<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Carbon\Carbon;
use App\Models\Asset;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;



class Inspection extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'inspection_id';
    protected $table = 'inspections';

    protected $fillable = [
        'inspection_asset_id', 'inspection_image_1', 'inspection_image_2', 'inspection_note', 'inspection_result', 'inspection_q_color', 'inspection_q_expiry', 'inspection_status', 'created_by', 'updated_by'
    ];

    protected $appends = ['created_at_format', 'updated_at_format'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y H:i');
	}

    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y H:i');
	}

    public function assetWiseInspection(){
        return $this->hasOne(Asset::class, 'asset_id', 'inspection_asset_id');
    }

    public function assetInfo()
    {
        return $this->belongsTo(Asset::class, 'inspection_asset_id', 'asset_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id')->withTrashed();
    }

    public function userName(){
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
