<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Carbon\Carbon;
use App\Models\SubSiteUser;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
//use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use LaravelAndVueJS\Traits\LaravelPermissionToVueJS;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Crypt;

class User extends Authenticatable
{
	use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles, LaravelPermissionToVueJS, GeneralTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'email',
		'employee_id_number',
		'notes',
		'client_company_name',
		'client_abn',
        'company_id',
		'client_uuid',
		'client_company_website',
		'client_company_contact_no',
		'client_representative_position',
		'client_company_logo',
		'password',
		'contact_number',
		'company',
		'website',
		'date_of_birth',
		'avatar',
		'address_line_1',
		'address_line_2',
		'street',
		'city',
		'state',
		'postal_code',
		'country',
		'status',
		'email_verified_at',
		'created_by',
		'updated_by',
		'is_confined_space',
        'is_root',
	];

	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array<int, string>
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array<string, string>
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	protected $appends = ['full_name', 'client_company_name_with_abn', 'created_at_format'];

	protected $guard_name = 'api';

	/**
     * Get the decrypted value of the name attribute.
     *
     * @param  string  $value
     * @return string
     */
    public function getFirstNameAttribute($value)
    {
		if ($value)
        	return Crypt::decryptString($value);
    }

    /**
     * Set the encrypted value of the name attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setFirstNameAttribute($value)
    {
		if($value)
        	$this->attributes['first_name'] = Crypt::encryptString($value);
    }

	/**
     * Get the decrypted value of the last name attribute.
     *
     * @param  string  $value
     * @return string
     */
    public function getLastNameAttribute($value)
    {
		if ($value)
        	return Crypt::decryptString($value);
    }

    /**
     * Set the encrypted value of the last name attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setLastNameAttribute($value)
    {
		if ($value)
        	$this->attributes['last_name'] = Crypt::encryptString($value);
    }

	/**
     * Get the encrypted value of the email attribute.
     *
     * @param  string  $value
     * @return string
     */
    public function getEmailAttribute($value)
    {
        return $this->decryptEmailString($value);
    }

    /**
     * Set the encrypted value of the email attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = $this->encryptEmailString($value);
    }

	/**
	 * Get User full name.
	 */
	public function getFullNameAttribute()
	{
		return ucwords("{$this->first_name} {$this->last_name}");
	}

	/**
	 * Get client company name with ABN.
	 */
	public function getClientCompanyNameWithAbnAttribute()
	{
		return $this->client_company_name . ' (' .  $this->client_abn . ')';
	}

	/**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
	}

    public function subSiteUser(){
        return $this->hasMany(SubSiteUser::class, 'ssu_user_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    /*
    *   Get Company data
    */
    public function companyData()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

	/*
    *   Get User location data
    */
	public function location(){
        return $this->hasMany(Site::class, 'company_id', 'company_id');
    }
}
