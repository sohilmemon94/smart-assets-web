<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Models\Module;



class Category extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'category_id';
    protected $table = 'category';

    protected $fillable = [
        'category_user_id', 'category_name', 'company_id', 'category_desc', 'category_is_default', 'category_status', 'created_by', 'updated_by'
    ];


    protected $appends = ['created_at_format', 'updated_at_format'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
	}

    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y');
	}

    public function module()
    {
        return $this->hasMany(Module::class, 'module_category_id', 'category_id');

    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    /*
    *   Get Company data
    */
    public function companyData()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
