<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use App\Models\Site;
use App\Models\SubSite;
use App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon\Carbon;

class ConfinedSpaceFormResult extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'csfr_id';
    protected $table      = 'confined_space_form_result';

    protected $fillable = [
        'csfr_site_id', 'csfr_sub_site_id', 'csfr_user_id', 'csfr_department', 'csfr_date', 'csfr_position', 'csfr_location_of_space', 'csfr_description_of_space', 'csfr_is_confined_space_number', 'csfr_confined_space_number', 'csfr_result', 'created_by', 'updated_by'
    ];

    protected $appends = ['csfr_date_format', 'created_at_format', 'updated_at_format'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y H:i');
	}

    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y H:i');
	}

    public function getCsfrDateFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->csfr_date))->format('d F Y');
	}

    public function location()
    {
        return $this->hasOne(Site::class, 'site_id', 'csfr_site_id');
    }

    public function area()
    {
        return $this->hasOne(SubSite::class, 'sub_site_id', 'csfr_sub_site_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'csfr_user_id');
    }

    public function answers()
    {
        return $this->hasMany(ConfinedSpaceAnswer::class, 'csa_csfr_id', 'csfr_id');
    }

}
