<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Module;
use App\Models\ModuleQuestion;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ModuleAnswer extends Model
{
    use HasFactory, GeneralTrait;

    protected $primaryKey = 'msa_id';
    protected $table      = 'module_survey_answers';

    protected $fillable = [
        'msa_question_id', 'msa_site_id', 'msa_sub_site_id', 'msa_module_id', 'msa_inspection_id', 'msa_user_id', 'msa_value','created_at', 'updated_at'
    ];

    protected $appends = ['created_at_format', 'updated_at_format'];

    /**
     * Get date as per require format.
     */
    public function getCreatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
    }

    public function getUpdatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y');
    }

    public function module()
    {
        return $this->hasOne(Module::class, 'module_id', 'msa_module_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'msa_user_id');
    }

    public function question()
    {
        return $this->hasOne(ModuleQuestion::class, 'msq_id', 'msa_question_id');
    }

    /*
    *   Get site data
    */
    public function site()
    {
        return $this->hasOne(Site::class, 'site_id', 'msa_site_id');
    }

    /*
    *   Get sub site data
    */
    public function subSite()
    {
        return $this->hasOne(SubSite::class, 'sub_site_id', 'msa_sub_site_id');
    }

}
