<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Models\ErrorLog;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\App;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {

            // Store data
            $errorData = array(
                'el_name'        => $e->getMessage(),
                'el_description' => $this->getFullErrorTrace($e),
                'el_type'        => 'web',
                'el_status'      => 'Y',
                'created_by'     => Auth::id()
            );

            $this->storeError($errorData);
        });
    }

    /**
     * Get the full error trace as a string.
     *
     * @param Throwable $e
     * @return string
     */
    protected function getFullErrorTrace(Throwable $e)
    {
        $trace = $e->getTraceAsString();

        // Check if the error has previous exceptions
        if ($e->getPrevious()) {
            $previousTrace = $this->getFullErrorTrace($e->getPrevious());
            $trace = $previousTrace . "\n" . $trace;
        }

        return $trace;
    }


    /**
     * Store mobile errors
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function storeError($errorData)
    {

        // store error log data
        ErrorLog::create($errorData);

        //Get all super admin email
        $superAdminEmails = User::where('first_name', '!=', '');
        $superAdminEmails = $superAdminEmails->whereHas("roles", function ($q) {
                                $q->where("name", "Super Admin");
                            })
                            ->pluck('email')
                            ->toArray();

        // Get the current environment
        $environment = App::environment();

        // Get the host name based on the environment
        $host = '';
        if ($environment === 'local') {
            // If it's the local environment, you can use the local host name
            $host = 'Localhost';
        } else {
            // For other environments, retrieve the host name from the URL facade
            $host = parse_url(URL::to('/'), PHP_URL_HOST);
        }

        $title = "New Error Log Recorded [$host - $environment]";

        Mail::raw("Error Message: ". $errorData['el_name'] ."\n\nError Trace:\n" . $errorData['el_description'], function ($message) use ($superAdminEmails, $title){
            $message->to($superAdminEmails)
                ->subject($title);
        });

        // Send New Error Log Mail Notification to super admin
        // $mail = Mail::send('reports.pdf.listingemail', array(
        //             'head'           => $title,
        //             'el_name'        => $errorData['el_name'],
        //             'el_description' => $errorData['el_description'],
        //             'el_type'        => 'web',
        //             'raised'         => "By {Auth::id()}   At " . Carbon::now()->format('d M Y H:i')
        //         ), function ($message) use ($superAdminEmails, $title) {
        //             $message->to($superAdminEmails)->subject($title);
        //         });

    }
}
