<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\MobileAPI\ApiController as MobileApiController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\Models\User; // Assuming you have a User model

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Test Email Routes
Route::get('welcome_mail/{email}',[TestController::class,'sendWelcomeMail']);
Route::get('report_mail/{email}',[TestController::class,'sendReportMail']);
Route::get('support_mail/{email}',[TestController::class,'sendSupportMail']);

Route::get('/reset-db', function() {
    // Remove all migration and run again
    \Artisan::call('migrate:refresh');

    // Run seeder
    \Artisan::call('db:seed');

    return 'Database refresh successfully';
});


Route::get('/encryptData', function() {
    // Fetch existing users from the database
    $users = User::withTrashed()->get();
    
    foreach ($users as $user) {
        // Encrypt name
        if($user->first_name != '') {
            $encryptedName = Crypt::encryptString($user->first_name);
            $user->first_name = $encryptedName;
        }
        
        // Encrypt last name
        if($user->last_name != '') {
            $encryptedLastName = Crypt::encryptString($user->last_name);
            $user->last_name = $encryptedLastName;
        }
        
        // Encrypt email
        $key = 'base64:Ka2wUTVLRhVPxePk7V1TJi+LRDo0GpC2K9B0/JvkXik=';
        $iv = '958353beb48c7711'; // Generate a random IV (Initialization Vector)
        $encrypted = openssl_encrypt($user->email, 'AES-256-CBC', $key, 0, $iv);
        $encodedIV = base64_encode($iv);
        $encodedEmail = base64_encode($encrypted);

        $user->email = $encodedIV . ':' . $encodedEmail;

        $user->save(); // Save the updated user record with encrypted data
    }

    echo "User data encrypted successfully.";
});

Route::get('/phpinfo', function () {
    phpinfo();
});

Route::get('/clear-cache', function() {
    // Routes cache cleared
    \Artisan::call('route:cache');

    // Config cache cleared
    \Artisan::call('config:cache');

    // Clear application cache
    \Artisan::call('cache:clear');

    // Clear view cache
    \Artisan::call('view:clear');

    // Clear cache using optimized class
    \Artisan::call('optimize:clear');

    return 'View cache cleared';
});

// Get json data if pass token param in mobile app
Route::get('asset-information/{asset_nfc_uuid}', [MobileApiController::class, 'assetDetail'])->name('asset-information');

Route::get('/decryptData', function() {
    // Fetch existing users from the database
    $users = User::withTrashed()->get();

    foreach ($users as $user) {
        $user->email = strtolower($user->email);
        $user->save();
    }

    echo "User data decrypted successfully.";
});


Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '.*');

