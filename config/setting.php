<?php

return [
    'WELCOME_EMAIL' => 'welcome@smartassettag.com.au',
    'REPORT_EMAIL' => 'reports@smartassettag.com.au',
    'SUPPORT_EMAIL' => 'support@smartassettag.com.au',

];
