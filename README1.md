# Recaptcha Functionality

## Introduction

- Recaptcha is a security measure used to protect web applications from automated bots and spam.
- By adding Recaptcha to your application, you can ensure that interactions are performed by real users rather than malicious bots.

## Getting Recaptcha API Keys

- To integrate Recaptcha into your application, follow these steps:
  - Visit the Recaptcha website.
  - Sign in or create a new account.
  - Register your application and obtain the Recaptcha API keys: the site key and the secret key.

## Frontend Implementation

- To include Recaptcha on the frontend of your application, follow these steps:

    - Add the Recaptcha script to your HTML file:

    ```html
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    ```

    - Add the Recaptcha widget to your form or page:

    ```html
        <div id="recaptchaContainer" ref="recaptchaContainer"></div>

        initializeRecaptcha() {
            const recaptchaContainer = document.getElementById('recaptchaContainer');
            const siteKey = '6LdOsj0mAAAAAFX2Lmssf4k3wSBIravKNWKqcQh6'; // Replace with your actual Site Key

            const widgetId = grecaptcha.render(recaptchaContainer, {
            sitekey: siteKey,
            callback: (response) => {
                this.onRecaptchaVerify(response);
            },
            'expired-callback': () => {
                this.onRecaptchaExpired();
            }
            });

            this.widgetId = widgetId;
        },
        onRecaptchaVerify(response) {
            if (response) {
            // reCAPTCHA verification succeeded
            // Proceed with the login process or enable the login button
            this.loginButtonDisabled = false;
            } else {
            this.loginButtonDisabled = true;
            // reCAPTCHA verification failed
            // Handle the error, such as displaying an error message
            this.showToast("Error", "XIcon", "danger", "reCAPTCHA verification failed. Please try again.");
            }
        },
        onRecaptchaExpired() {
            // Handle the reCAPTCHA expiration
            // Disable the login button or display an error message
            this.showToast("Error", "XIcon", "danger", "Verification expired. Check the checkbox again.");
            this.loginButtonDisabled = true; // Disable the login button
        },
    ```
## Note : 

- Make sure to include the necessary dependencies for reCAPTCHA, such as the reCAPTCHA script and its associated JavaScript file. 
- Additionally, ensure that the initializeRecaptcha() function is called appropriately to initialize the reCAPTCHA widget on your page.

## Best Practices and Security Considerations

- When implementing Recaptcha, keep the following best practices in mind:

    - Place the Recaptcha widget in visible and easily accessible locations, such as login or registration forms.
    - Avoid placing the secret key in frontend code to prevent exposure.
    - Regularly monitor and update your Recaptcha integration to stay up to date with any changes or improvements.


# 2 Factor Authentication

## Introduction

- Two-Factor Authentication (2FA) adds an extra layer of security to your application, you can significantly enhance the security of user accounts.

## 2FA Methods

- There are several methods for implementing 2FA. The most common ones include:
    - One-Time Passwords (OTP): Users receive a temporary OTP via SMS, email, or authenticator apps like Google Authenticator.
    - Push Notifications: Users receive a push notification on their registered device to approve the login request.
    - Biometric Authentication: Users authenticate using fingerprint, face recognition, or other biometric methods.

## One-Time Passwords (OTP):

- Frontend code:
    - Add a 2FA setup page where users can enable and configure 2FA.
    - Display a 2FA verification form on the login page after users enter their credentials.
    - Provide clear instructions and visual cues for users to enter the second factor (e.g., OTP field).

    ```html
        <!-- START :: OTP -->
        <b-col lg="4" class="d-flex align-items-center auth-bg px-2 p-lg-5" v-if="isOTPScreen">
            <b-col sm="8" md="6" lg="12" class="px-xl-2 mx-auto">
                <b-card-title title-tag="h2" class="font-weight-bold mb-1">
                <b-img fluid :src="imgUrl" alt="Login V2" class="mobileLogo" />
                <div class="text-center">Welcome to</div>
                <div class="text-center">{{ projectName }} Tag</div>
                </b-card-title>
                <b-card-text class="mb-2">
                Please sign in to your account with the One-Time Password (OTP) received in your email.
                </b-card-text>

                <validation-observer ref="loginOTPForm">
                <b-form class="auth-login-form mt-2">
                    <validation-provider #default="validationContext" name="OTP" rules="required">
                    <b-form-group label-for="login-otp">
                        <label>Enter OTP <span class="text-danger">*</span></label>
                        <b-form-input id="login-otp" v-model="userData.userOTP" :state="getValidationState(validationContext)" trim></b-form-input>
                        <b-form-invalid-feedback>{{ validationContext.errors[0] }}</b-form-invalid-feedback>
                    </b-form-group>
                    </validation-provider>

                    <div class="d-flex justify-content-between align-items-center mb-2">
                    <p v-if="showCountdown" class="mb-0">Resend OTP in {{ countdownMinutes }}:{{ countdownSeconds }}</p>
                    <b-button variant="flat-secondary" :disabled="!resendOTPEnabled" @click="resendOTP" class="ml-auto">Resend OTP</b-button>
                    </div>

                    <b-button variant="primary" block @click="validateOtp">Validate OTP</b-button>
                </b-form>
                </validation-observer>
            </b-col>
        </b-col>
        <!-- END :: OTP -->

        validateOtp() {
            this.$refs.loginOTPForm.validate().then((success) => {
            if (success) {
                AuthServices.validateLoginOtp({email: this.userEmail, userOtp: this.userData.userOTP}).then((response) => {
                if (response.status === true || response.status == "true") {
                    if (response.data.otp == this.userData.userOTP) {
                    if(this.loginUserData) {
                        // NOTE :: Split code due to not repeat code when super admin code repeat
                        this.setUserData(this.loginUserData);
                    }
                    }
                } else {
                    this.showToast("Error", "XIcon", "danger", response.message);
                }
                return Promise.resolve(response);
                });
            }
            });
        },
        resendOTP() {
            // Reset the countdown timer and disable the "Resend OTP" button
            this.startCountdownTimer();
            this.resendOTPEnabled = false;

            // Make an API request to the server to resend the OTP
            // Resend otp to super admin
            AuthServices.resendLoginOtp({email: this.userEmail}).then((response) => {
            if (response.status === true || response.status == "true") {
                this.showToast("Welcome", "CheckIcon", "success", "OTP email resend successfully.");
            }
            return Promise.resolve(response);
            });
        },
        startCountdownTimer() {
            this.countdown = 60;
            this.resendOTPEnabled = false;

            this.countdownTimer = setInterval(() => {
            if (this.countdown > 0) {
                this.countdown--;
            } else {
                clearInterval(this.countdownTimer);
                this.resendOTPEnabled = true;
            }
            }, 1000);
        },
    ```

- Backend Implementation
    - Generate and store a unique secret key for each user.
    - When a user logs in, prompt them for their second factor (e.g., OTP).
    - Verify the entered second factor against the stored secret key.
    - If the second factor is valid, allow the user to proceed with authentication.

    ```html
        public function generateOtp($email)
        {
            $user = User::where('email', $email)->first();

            # User Does not Have Any Existing OTP
            $verificationCode = VerificationCode::where('user_id', $user->id)->latest()->first();

            $now = Carbon::now();

            if($verificationCode && $now->isBefore($verificationCode->expire_at)){
                return $verificationCode;
            }

            // Create a New OTP
            return VerificationCode::create([
                'user_id'   => $user->id,
                'otp'       => rand(123456, 999999),
                'expire_at' => Carbon::now()->addMinutes(15)
            ]);
        }

        /**
        * Resend login otp
        *
        * @return [json] object
        */
        public function resendLoginOtp(Request $request)
        {
            // Get user data
            $user = User::where('email', $request->email)->first();

            // Generate OTP
            $verificationCode = $this->generateOtp($request->email);

            // Send otp email to super admin user
            $mail = new LoginOtpMail($verificationCode->otp, $user->first_name);
            Mail::mailer('support')->to($user->email)->send($mail);
        }

        /**
        * Validate login otp
        *
        * @return [json] object
        */
        public function validateLoginOtp(Request $request)
        {
            $request->merge(['email' => $this->encryptEmailString($request->email)]);

            $user = User::where('email', $request->email)->first();

            if($user) {
                # User Does not Have Any Existing OTP
                $verificationCode = VerificationCode::where('user_id', $user->id)->latest()->first();

                $now = Carbon::now();
        
                if($verificationCode && $now->isBefore($verificationCode->expire_at)) {
                    if($verificationCode->otp == $request->userOtp) {
                        return $this->returnSuccessMessage(null, $verificationCode);
                    } else {
                        return $this->returnError(200, 'You entered an incorrect OTP. Please try again.');
                    }
                } else {
                    return $this->returnError(200, 'The OTP has expired. Please request a new OTP.');
                }
            }
        }
    ```