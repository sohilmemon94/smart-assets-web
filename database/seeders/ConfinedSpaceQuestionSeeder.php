<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\ConfinedSpaceQuestion;


class ConfinedSpaceQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csqData = [
            ['csq_parent_id' => 0, 'csq_title' => 'Is the space enclosed or partially enclosed?', 'created_by' => '1'],
            ['csq_parent_id' => 0, 'csq_title' => 'Is the space not designed or intended primarily to be occupied by a person?', 'created_by' => '1'],
            ['csq_parent_id' => 0, 'csq_title' => 'Is the space designed or intended to be at normal pressure while any person is in the space?', 'created_by' => '1'],
            ['csq_parent_id' => 0, 'csq_title' => 'The space is NOT a mine shaft or the workings of a mine?', 'created_by' => '1'],
            ['csq_parent_id' => 0, 'csq_title' => 'Is the space likely to be a risk to health and safety from:', 'created_by' => '1'],
            ['csq_parent_id' => 5, 'csq_title' => 'An atmosphere that does not have a safe oxygen level?', 'created_by' => '1'],
            ['csq_parent_id' => 5, 'csq_title' => 'Contaminants, including airborne gases, vapours and dusts that may cause injury from fire or explosion?', 'created_by' => '1'],
            ['csq_parent_id' => 5, 'csq_title' => 'Harmful concentrations of any airborne contaminants?', 'created_by' => '1'],
            ['csq_parent_id' => 5, 'csq_title' => 'Engulfment?', 'created_by' => '1'],
        ];

        foreach ($csqData as $data) {
            ConfinedSpaceQuestion::create($data);
        }
    }
}
