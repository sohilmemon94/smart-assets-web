<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id('asset_id');
            $table->unsignedBigInteger('asset_user_id');
            $table->unsignedBigInteger('asset_site_id');
            $table->unsignedBigInteger('asset_sub_site_id');
            $table->unsignedBigInteger('asset_module_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('asset_user_id')->references('id')->on('users');
            $table->foreign('asset_site_id')->references('site_id')->on('locations');
            $table->foreign('asset_sub_site_id')->references('sub_site_id')->on('areas');
            $table->foreign('asset_module_id')->references('module_id')->on('modules');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
};
