<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company', function (Blueprint $table) {
            $table->date('trail_start_date')->nullable()->after('commence_date');
            $table->text('notes')->after('trail_start_date')->nullable();
            $table->string('url_to_quotation')->after('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            $table->dropColumn('trail_start_date');
            $table->dropColumn('notes');
            $table->dropColumn('trail_start_date');
        });
    }
};
