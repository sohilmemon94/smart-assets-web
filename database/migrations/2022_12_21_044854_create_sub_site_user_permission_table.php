<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_site_user_permission', function (Blueprint $table) {
            $table->id('ssup_id');
            $table->unsignedBigInteger('ssup_sub_site_id');
            $table->unsignedBigInteger('ssup_user_id');
            $table->unsignedBigInteger('ssup_category_id');
            $table->unsignedBigInteger('ssup_module_id');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();


            // Foreign Key
            $table->foreign('ssup_sub_site_id')->references('sub_site_id')->on('areas')->onDelete('cascade');
            $table->foreign('ssup_category_id')->references('category_id')->on('category')->onDelete('cascade');
            $table->foreign('ssup_module_id')->references('module_id')->on('modules')->onDelete('cascade');
            $table->foreign('ssup_user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_site_user_permission');
    }
};
