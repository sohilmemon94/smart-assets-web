<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_clone_category', function (Blueprint $table) {
            $table->id('cca_id');
            $table->unsignedBigInteger('cca_user_id');
            $table->unsignedBigInteger('cca_clone_category_id');
            $table->unsignedBigInteger('cca_new_category_id');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('cca_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('cca_clone_category_id')->references('category_id')->on('category')->onDelete('cascade');
            $table->foreign('cca_new_category_id')->references('category_id')->on('category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_clone_category');
    }
};
