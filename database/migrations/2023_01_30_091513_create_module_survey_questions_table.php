<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_survey_questions', function (Blueprint $table) {
            $table->id('msq_id');
            $table->unsignedBigInteger('msq_module_id');
            $table->unsignedBigInteger('msq_user_id');
            $table->string('msq_title');
            $table->text('msq_desc')->nullable();
            $table->json('msq_options')->nullable();
            $table->enum('msq_type', ['radio', 'multiselect'])->default('radio');
            $table->enum('msq_is_default', ['Y', 'N'])->default('N')->comment('Y => Yes, N => No');
            $table->enum('msq_type_has_button', ['Y', 'N'])->default('N')->comment('Y => Active, N => Inactive');
            $table->enum('msq_has_required', ['Y', 'N'])->default('N')->comment('Y => Active, N => Inactive');
            $table->enum('msq_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('msq_module_id')->references('module_id')->on('modules')->onDelete('cascade');
            $table->foreign('msq_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_survey_questions');
    }
};
