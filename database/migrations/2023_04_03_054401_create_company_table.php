<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('abn', 20);
            $table->string('client_id', 20);
            $table->string('website', 120);
            $table->string('contact_no', 15);
            $table->string('logo', 50);
            $table->string('street', 50);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->string('country', 50);
            $table->string('postal_code', 20);
            $table->enum('is_confined_space', ['Y', 'N'])->default('N')->comment('Y => Yes, N => No');
            $table->enum('status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
};
