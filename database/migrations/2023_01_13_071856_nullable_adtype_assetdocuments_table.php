<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_documents', function (Blueprint $table) {
            $table->string('ad_type')->nullable()->change();
            $table->string('ad_file')->nullable()->change();
            $table->string('ad_size')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_documents', function (Blueprint $table) {
            $table->string('ad_type')->nullable(false)->change();
            $table->string('ad_file')->nullable(false)->change();
            $table->string('ad_size')->default(null)->change();
        });
    }
};
